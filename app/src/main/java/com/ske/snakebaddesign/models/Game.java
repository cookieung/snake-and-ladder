package com.ske.snakebaddesign.models;

import android.content.DialogInterface;
import android.util.Log;

import java.util.Observable;
import java.util.Random;

/**
 * Created by Salilthip on 3/16/2016.
 */
public class Game extends Observable{

    Player player1 = new Player(new Piece());
    Player player2 = new Player(new Piece());

    Die die;

    int turn = 0, boardSize = 0;

    public Game(int boardSize){
        die = new Die();
        this.boardSize = boardSize;
    }

    public int rollDie(){
        return die.roll();
    }

    public int adjustPosition(int current, int distance) {
        current = current + distance;
        int maxSquare = boardSize * boardSize - 1;
        if(current > maxSquare) {
            current = maxSquare - (current - maxSquare);
        }
        return current;
    }

    public void resetGame() {
        turn = 0;
        player1.getPiece().setPosition(0);
        player2.getPiece().setPosition(0);
    }

    public String checkWin(){
        if (player1.getPiece().getPosition() == boardSize * boardSize - 1) {
            return  "Player 1 won!";
        } else if (player2.getPiece().getPosition() == boardSize * boardSize - 1) {
            return  "Player 2 won!";
        }
        return "";
    }

    public void moveCurrentPiece(int value) {
        if (turn % 2 == 0) {
            player1.getPiece().setPosition(adjustPosition(player1.getPiece().getPosition(), value));
        } else {
            player2.getPiece().setPosition(adjustPosition(player2.getPiece().getPosition(), value));
        }
        setChanged();
        notifyObservers(turn);
        nextTurn();
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void nextTurn() {
        turn++;
    }

    public int getTurn() {
        return turn;
    }
}
