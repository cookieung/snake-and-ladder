package com.ske.snakebaddesign.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Salilthip on 3/16/2016.
 */
public class Board {

    int size;
    List<Square>  squares;

    public Board(int size){
        this.size = size;
        squares = new ArrayList<Square>();
    }

    public void setBoardSize(int size){
        this.size = size;
    }

    public int getBoardSize(){
        return size;
    }

}
