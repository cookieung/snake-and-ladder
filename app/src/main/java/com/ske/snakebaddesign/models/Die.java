package com.ske.snakebaddesign.models;

/**
 * Created by Salilthip on 3/16/2016.
 */
public class Die {


    int[] face = {1,2,3,4,5,6};

    public Die(){

    }

    public int roll(){
        return face[(int)(Math.random() * 6) ];
    }

}
