# SNAKE & LADDER #

A application of simple snake & ladder that have 2 players and one die. The winner is a player that arrive the the goal first.

### Domain Model ###
![domain model-snake - New Page.jpeg](https://bitbucket.org/repo/z4oz4q/images/509729074-domain%20model-snake%20-%20New%20Page.jpeg)

### How about GRASP ###

* Pure Fabrication
    * Make the method act only its duty.
* Controller
    * Create Game class for control the UI.
    * GameActivity is a main UI that has BoardView.
    * BoardView is a UI of board.
* Creator
    * Game create its player and its die.
    * Player create its piece.
* Indirection
    * GameActivity use board by BoardView that have the board.

### What is changed ###

* Edit GameActivity by Observer Pattern
* Create models separate from the original GameActivity
    * Game
    * Board
    * Player
    * Die
    * Piece
    * Square